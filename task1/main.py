import os


exchange_rate = 100


def rub_to_usd(rubles):
    return rubles / exchange_rate


def usd_to_rub(dollars):
    return dollars * exchange_rate


def convert_currency():
    currency = os.environ.get("CURRENCY")
    amount = float(os.environ.get("AMOUNT"))

    if currency == "RUB":
        converted_amount = rub_to_usd(amount)
        print(f"{amount} RUB is equivalent to {converted_amount:.2f} USD.")
    elif currency == "USD":
        converted_amount = usd_to_rub(amount)
        print(f"{amount} USD is equivalent to {converted_amount:.2f} RUB.")
    else:
        print("Invalid currency. Please enter RUB or USD.")


convert_currency()
